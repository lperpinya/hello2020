package com.exemple.profe.helloworld2020;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String hello = getString (R.string.hello);
        String nomAplicacio = getString (R.string.app_name);


        int diaSemana = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        diaSemana--;
        //Calendar.Day_of_week retorna un nombre entre 1 i 7. Ens interessa entre 0 i 6
        String[] diesSetmana = getResources().getStringArray (R.array.day_of_week);
        String salutacio = hello + " " + diesSetmana[diaSemana];
        Toast.makeText (this, salutacio, Toast.LENGTH_LONG).show();
        // Falta explicar com fer l'aplicació multiidioma i modificar el TextView
        TextView tvHello = findViewById (R.id.tvhello);
        tvHello.setText(hello);







    }
}
